package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

func main() {
	datadir := flag.String("d", "data", "selinux-patchwork data directory")
	repodir := flag.String("r", "selinux", "selinux repository")
	download := flag.Bool("download", false, "sync with the internet")

	flag.Parse()

	if *download {
		if err := os.Chdir(*repodir); err != nil {
			fmt.Println("git", "clone", "https://github.com/SELinuxProject/selinux", *repodir)
			cmd := exec.Command("git", "clone", "https://github.com/SELinuxProject/selinux", *repodir)
			err = cmd.Run()
			if err != nil {
				panic(err)
			}
			os.Chdir(*repodir)
		} else {
			os.Chdir(*repodir)
			fmt.Println("git", "pull")
			cmd := exec.Command("git", "pull")
			if err = cmd.Run(); err != nil {
				panic(err)
			}
		}
	} else {
		os.Chdir(*repodir)
	}

	gitLog := make([]string, 0)
	cmdGitLog := exec.Command("git", "log", "--pretty=oneline", "--no-decorate")

	pipeG, _ := cmdGitLog.StdoutPipe()
	scannerG := bufio.NewScanner(pipeG)
	if err := cmdGitLog.Start(); err != nil {
		panic(err)
	}
	for scannerG.Scan() {
		gitLog = append(gitLog, scannerG.Text())
	}
	cmdGitLog.Wait()

	var cmd *exec.Cmd
	if *download {
		cmd = exec.Command("selinux-patchwork", "-d", *datadir, "-f")
	} else {
		cmd = exec.Command("selinux-patchwork", "-d", *datadir)
	}
	pipeP, _ := cmd.StdoutPipe()
	scannerP := bufio.NewScanner(pipeP)
	if err := cmd.Start(); err != nil {
		panic(err)
	}

	for scannerP.Scan() {
		line := scannerP.Text()

		// https://patchwork.kernel.org/patch/12790631/ New [v3] libsemanage: Fall back to semanage_copy_dir when rename() fails
		var pw = regexp.MustCompile("^[^ ]*/([^/]+)/ New (\\[[^\\]]+\\] )?(.*)$")
		var matches = pw.FindStringSubmatch(line)
		if matches == nil {
			continue
		}
		var patch_id = matches[1]
		var message = matches[3]
		for _, log := range gitLog {
			if strings.Contains(log, message) {
				commit_id := strings.SplitN(log, " ", 2)[0]
				fmt.Printf("# %s\npwclient update -c %s -s Accepted %s\n", message, commit_id, patch_id)
			}
		}
	}

}
