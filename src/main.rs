
use std::fmt;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::process;
use std::process::Command;

// https://docs.rs/clap/2.33.0/clap/
// https://raw.githubusercontent.com/clap-rs/clap/master/examples/01a_quick_example.rs
extern crate clap;

use clap::{App, Arg};
use glob::glob;

#[derive(Debug)]
struct Patch {
    id: String,
    state: String,
    name: String,
    //    directory: String,
    //    msgid: String,
}

impl fmt::Display for Patch {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{} {} {}", self.id, self.state, self.name)
    }
}

impl Patch {
    fn new(id: &str, state: &str, name: &str) -> Self {
        Patch {
            id: id.to_string(),
            state: state.to_string(),
            name: name.to_string(),
            //            directory: String::new(),
        }
    }

    fn download_patch(&self) -> Result<String, String> {
        if std::path::Path::new(&self.id).exists() {
            println!("Directory {} already exists, skipping", &self.id);
            return Ok(String::from("skipped"));
        }
        match fs::create_dir_all(&self.id) {
            Ok(directory) => directory,
            Err(_) => return Err("Cannot create directory for download".to_string()),
        };

        let current_dir = match std::env::current_dir() {
            Ok(dir) => dir,
            Err(_) => return Err("Cannot get current directory".to_string()),
        };

        if std::env::set_current_dir(Path::new(&self.id)).is_err() {
            return Err("Cannot set current directory".to_string());
        }

        Command::new("pwclient")
            .arg("get")
            .arg(&self.id)
            .output()
            .expect("Failed to spawn pwclient child process");

        if std::env::set_current_dir(Path::new(&current_dir)).is_err() {
            return Err("Cannot set directory back".to_string());
        }

        Ok(String::from("downloaded"))
    }

    fn prepare_directory(&self) -> Result<String, String> {
        match fs::create_dir_all(&self.id) {
            Ok(_) => Ok(String::from(&self.id)),
            Err(why) => {
                println!("! {:?}", why.kind());
                Err(why.to_string())
            }
        }
    }

    fn read_patch(&self) -> Result<String, String> {
        let current_dir = match std::env::current_dir() {
            Ok(dir) => dir,
            Err(_) => return Err("Cannot get current directory".to_string()),
        };

        if std::env::set_current_dir(Path::new(&self.id)).is_err() {
            return Err("Cannot set current directory".to_string());
        }

        match fs::read_dir("./") {
            Err(why) => panic!("! {:?}", why.kind()),
            Ok(mut paths) => {
                let path = match paths.next() {
                    Some(path) => match paths.next() {
                        None => path,
                        _ => panic!("Too many files in the patch the directory"),
                    },
                    None => panic!("File not found"),
                };
                // println!("> {:?}", path.unwrap().path());
                let mut file = match File::open(&path.unwrap().path()) {
                    // The `description` method of `io::Error` returns a string that
                    // describes the error
                    Err(why) => panic!("couldn't open {}", why.to_string()),
                    Ok(file) => file,
                };

                // Read the file contents into a string, returns `io::Result<usize>`
                let mut s = String::new();
                file.read_to_string(&mut s).unwrap();
                if std::env::set_current_dir(Path::new(&current_dir)).is_err() {
                    return Err("Cannot set directory back".to_string());
                };
                Ok(s)
            }
        }
    }

    fn matches(&self, patterns: &[&str]) -> bool {
        let patch_patterns: Vec<String> = patterns
            .iter()
            .map(|p| format!("--- a/{p}", p = p))
            .collect();
        let patch_data = match self.read_patch() {
            Ok(data) => data,
            Err(_) => return false,
        };
        for line in patch_data.lines() {
            for p in &patch_patterns {
                if let Some(0) = line.find(p.as_str()) {
                    return true;
                }
            }
        }
        false
    }

    fn weblink(&self) -> String {
        format!("https://patchwork.kernel.org/patch/{}/", self.id)
    }
}

fn get_patches(cached: bool, archived: bool) -> Vec<Patch> {
    let output_data = if cached {
        let path = Path::new("patch.list");
        let mut file = match File::open(&path) {
            Err(why) => panic!("couldn't open {}: {}", path.display(), why.to_string()),
            Ok(file) => file,
        };

        // Read the file contents into a string, returns `io::Result<usize>`
        let mut s = String::new();
        match file.read_to_string(&mut s) {
            Err(why) => panic!("couldn't read {}: {}", path.display(), why.to_string()),
            Ok(_) => s,
        }
    } else {
        let output = match archived {
            true => Command::new("pwclient")
                .arg("list")
                .arg("-s")
                .arg("New")
                .arg("-a")
                .arg("yes")
                .arg("-f")
                .arg("%{id};%{state};%{name}")
                .output()
                .expect("Failed to spawn pwclient child process"),
            false => Command::new("pwclient")
                .arg("list")
                .arg("-s")
                .arg("New")
                .arg("-a")
                .arg("no")
                .arg("-f")
                .arg("%{id};%{state};%{name}")
                .output()
                .expect("Failed to spawn pwclient child process")
        };
        if !output.status.success() {
            panic!(
                "pwclient child process failed with: {}",
                String::from_utf8(output.stderr).unwrap()
            );
        }
        let output_data = String::from_utf8(output.stdout).unwrap();
        match save_data("patch.list".to_string(), &output_data) {
            Ok(output) => println!("{}", output),
            Err(output) => print!("{}", output),
        };
        output_data
    };

    let mut result = Vec::new();
    let lines = output_data.as_str().lines();
    for line in lines {
        // let line = lines.next().unwrap();
        let mut index = line.find(';').unwrap();
        let (id, line_state_name) = line.split_at(index);
        index = line_state_name[1..].find(';').unwrap();
        let (state, line_name) = line_state_name[1..].split_at(index);
        let (_, name) = line_name.split_at(1);

        // dbg!(id, state, name);
        result.push(Patch::new(id, state, name));
    }
    result
}

fn save_data(file: String, data: &str) -> Result<String, String> {
    let path = Path::new(&file);
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why.to_string()),
        Ok(file) => file,
    };

    match file.write_all(data.as_bytes()) {
        Err(why) => {
            println!("couldn't write to {}: {}", display, why.to_string());
            Err(why.to_string())
        }
        Ok(_) => {
            println!("successfully wrote to {}", display);
            Ok("successfully wrote".to_string())
        }
    }
}

fn download_all_patches(patches: &[Patch]) -> bool {
    for patch in patches {
        match patch.download_patch() {
            Ok(output) => println!("Created {} - {}", &patch.id, output),
            Err(output) => println!("{}", output),
        };
    }
    true
}

fn sync_patches(patches: &[Patch]) -> bool {
    let files: Vec<PathBuf> = glob("[0-9]*").unwrap().map(|x| x.unwrap()).collect();
    let ids: Vec<PathBuf> = patches.iter().map(|x| PathBuf::from(&x.id)).collect();
    for f in files {
        if !ids.contains(&f) {
            println!("lets remove {}", f.to_str().unwrap());
            fs::remove_dir_all(&f).unwrap();
        }
    }
    for patch in patches {
        match patch.download_patch() {
            Ok(output) => println!("{} - {}", &patch.id, output),
            Err(output) => println!("{}", output),
        };
    }
    true
}

// cargo test -- --nocapture  --test-threads=1
#[cfg(test)]
mod tests {
    use super::*;

    static DATA_DIR: &'static str = "tests";
    static TEST_DATA_DIR: &'static str = "data-tests";

    fn _file_name(path: &Path) -> Option<String> {
        path.file_name().and_then(|s| s.to_str()).map(String::from)
    }

    fn _prepare_test_directory() {
        let status = Command::new("/bin/cp")
            .arg("-r")
            .arg(DATA_DIR)
            .arg(TEST_DATA_DIR)
            .status()
            .expect("failed to copy test data");
        assert!(status.success());
    }

    #[test]
    fn test_get_patches() {
        let patches = get_patches(&DATA_DIR.to_string(), true);
        assert_ne!(patches.len(), 0);
    }

    #[test]
    fn test_patch_read_patch() {
        let data_dir = &DATA_DIR.to_string();
        let test_data_dir = &TEST_DATA_DIR.to_string();

        _prepare_test_directory();
        let patches = get_patches(&test_data_dir, true);
        assert_ne!(patches.len(), 0);

        let s = patches[0].read_patch();
        assert!(s.is_ok());
        fs::remove_dir_all(&test_data_dir).unwrap();
    }

    #[test]
    fn test_patch_prepare_directory() {
        let data_dir = &DATA_DIR.to_string();
        let test_data_dir = &TEST_DATA_DIR.to_string();
        let patches = get_patches(&data_dir, true);
        assert_ne!(patches.len(), 0);
        let test_patch = Patch {
            directory: test_data_dir.clone(),
            id: patches[0].id.clone(),
            state: patches[0].id.clone(),
            name: patches[0].id.clone(),
        };
        assert!(test_patch.prepare_directory().is_ok());
        fs::remove_dir_all(&test_data_dir).unwrap();
    }

    #[test]
    fn test_patch_matches() {
        _prepare_test_directory();
        let patch = Patch::new(
            "0004",
            "New",
            "use clap for command line options",
            &TEST_DATA_DIR,
        );
        // assert!(_prepare_directory(&data_dir, &patch).is_ok());
        let mut patterns = vec!["src"];
        assert!(patch.matches(&patterns));
        patterns = vec!["random"];
        assert!(!patch.matches(&patterns));

        fs::remove_dir_all(&TEST_DATA_DIR).unwrap();
    }
}

fn main() {
    // let data_dir = String::from("data");

    let matches = App::new("selinux-patchwork")
        .version("0.1.1")
        .author("Petr Lautrbach <brs@o5za5.cz>")
        .about("Download and filer patches from patchwork.kernel.org")
        .arg(
            Arg::with_name("datadir")
                .short("d")
                .long("datadir")
                .value_name("datadir")
                .help("Sets a custom data directory")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("cached")
                .short("f")
                .long("fetch")
                .value_name("cached")
                .help("Fetch patches from patchwork")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("archived")
                .short("a")
                .long("archived")
                .value_name("archived")
                .help("Fetch patches from patchwork")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("pattern")
                .short("p")
                .long("pattern")
                .value_name("pattern")
                .multiple(true)
                .number_of_values(1)
                .takes_value(true)
                .help("Patterns to match in a patch"),
        )
        .get_matches();

    let data_dir = matches.value_of("datadir").unwrap_or("data");
    let cached = !matches.is_present("cached");
    let archived = matches.is_present("archived");
    let patterns: Vec<&str> = match matches.values_of("pattern") {
        Some(patterns) => patterns.collect(),
        None => vec![
            "libsepol",
            "libselinux",
            "libsemanage",
            "policycoreutils",
            "checkpolicy",
            "secilc",
            "dbus",
            "gui",
            "mcstrans",
            "python",
            "sandbox",
            "restorecond",
            "scripts",
            "semodule-utils",
            "secilc",
        ],
    };

    if std::env::set_current_dir(Path::new(data_dir)).is_err() {
        println!("Cannot change directory to {}", data_dir);
        process::exit(1);
    }

    let patches = get_patches(cached, archived);

    if !cached {
        // download_all_patches(&patches);
        sync_patches(&patches);
    }
    for patch in patches.iter().filter(|p| p.matches(&patterns)) {
        println! {"{} {} {}", patch.weblink(), patch.state, patch.name};
    }
}
