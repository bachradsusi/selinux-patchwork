# selinux-patchwork

A command line tool for downloading and filtering patches from https://patchwork.kernel.org/project/selinux/list/

By default, './data' is used as a local cache for patches, it can be change using `-d <datadir>`.

Patches are downloaded only if `-f` is used.

Patches are filtered based on patterns in format `--- a/{pattern}/` in patch.
E.g. `-p tests` would look for `--- a/tests/` in a patch and when it's found, the patch is used for output.
Default patterns corresponds to: `-p libsepol -p libselinux -p libsemanage -p policycoreutils -p checkpolicy -p secilc`

# Usage

```
USAGE:
    selinux-patchwork [FLAGS] [OPTIONS]

FLAGS:
    -f, --fetch      Fetch patches from patchwork
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --datadir <datadir>       Sets a custom data directory
    -p, --pattern <pattern>...    Patterns to match in a patch

```

# Examples

```
^&^ selinux-patchwork 
https://patchwork.kernel.org/patch/10905761/ New libselinux: Ignore the stem when looking up all matches in file context
https://patchwork.kernel.org/patch/10971701/ New [V2] selinux: Fix strncpy in libselinux and libsepol
https://patchwork.kernel.org/patch/10993621/ New libsepol: error in CIL if a permission cannot be resolved
https://patchwork.kernel.org/patch/11027771/ New [2/2] Fix mcstrans secolor examples
https://patchwork.kernel.org/patch/11077727/ New [1/1] libsemanage: include internal header to use the hidden function prototypes
https://patchwork.kernel.org/patch/11077787/ New [1/1] libselinux: ensure that digest_len is not zero
```

```
^&^ selinux-patchwork -p policy -p tests
https://patchwork.kernel.org/patch/10972719/ New [RFC,V3] selinux-testsuite: Add test for restorecon
https://patchwork.kernel.org/patch/11038601/ New selinux-testsuite: add tests for fsnotify
https://patchwork.kernel.org/patch/11070519/ New [V2,1/2] selinux-testsuite: Add BPF tests
https://patchwork.kernel.org/patch/11070533/ New selinux-testsuite: add tests for fsnotify
https://patchwork.kernel.org/patch/11093559/ New [V3,1/2] selinux-testsuite: Add BPF tests
https://patchwork.kernel.org/patch/11093563/ New [V3,2/2] selinux-testsuite: Add BPF support to fdreceive test
```
